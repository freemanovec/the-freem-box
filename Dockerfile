FROM python:3.10-bookworm

ARG POETRY_VERSION=1.7.1
ENV POETRY_HOME=/usr/local
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_INSTALLER_PARALLEL=false

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
	build-essential \
	gcc \
	musl-dev \
	portaudio19-dev \
	libsndfile1 \
	git \
	ffmpeg \
	curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
RUN curl -sSL https://install.python-poetry.org | python3.10 -

WORKDIR /app
COPY poetry.lock pyproject.toml /app/

RUN poetry install --sync

COPY . .

ENTRYPOINT [ "python3.10", "-u", "-m", "backend" ]
