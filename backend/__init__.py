from typing import Any, List
import logging

import grpc.aio

import backend.analysis
import backend.configuration
import backend.servicers.audio
import backend.servicers.spotify


class Backend:
	def __init__(self, configuration: backend.configuration.Configuration) -> None:
		self._logger = logging.getLogger('backend')

		self._configuration = configuration
		self._analysis_engine = backend.analysis.AnalysisEngine(self._configuration)

		self._server: grpc.aio.Server = grpc.aio.server()
		self._servicers: List[Any] = [
			backend.servicers.spotify.SpotifyServicer(self._server, self._analysis_engine),
			backend.servicers.audio.AudioServicer(self._server, self._analysis_engine),
		]

		# still need to implement
		# 	/radio/<string:spotify_id> - radio_endpoint # probably make a relay application, stream RIFF via gRPC

		self._server.add_insecure_port('[::]:50051')

	async def run(self):
		await self._server.start()
		self._logger.info('Server started')
		await self._server.wait_for_termination()
