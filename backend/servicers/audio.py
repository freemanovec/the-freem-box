# generated stubs are typed for synchronous gRPC
# pylint: disable=invalid-overridden-method

from typing import AsyncIterable, List, Optional
import logging
import math

import freem_protocols.freem_box.audio_pb2 as pb_audio
import freem_protocols.freem_box.audio_pb2_grpc as pb_audio_svcs
import grpc.aio

import backend.analysis
import backend.analysis.parts
import backend.radio.driver


class AudioServicer(pb_audio_svcs.AudioServiceServicer):
	def __init__(self, server: grpc.aio.Server, analysis_engine: backend.analysis.AnalysisEngine) -> None:
		super().__init__()
		pb_audio_svcs.add_AudioServiceServicer_to_server(self, server)

		self._analysis_engine = analysis_engine
		self._logger = logging.getLogger('grpc-service-audio')

	async def StreamDiscordRadio(
		self,
		request: pb_audio.DiscordRadioStreamRequest,
		_,
	) -> AsyncIterable[pb_audio.DiscordRadioStreamResponse]:

		request_parameters = request.parameters

		self._logger.info('Parsing parameters for "%s"', request.id)
		parameters = backend.analysis.parts.AnalysisParameters(
			jump_threshold_min = request_parameters.bound_lower,
			jump_threshold_max = request_parameters.bound_higher,
			jump_threshold_increment = request_parameters.ramp_up_speed,
			just_backwards = request_parameters.only_reverse,
			just_long_branches = request_parameters.only_long,
			current_threshold = request_parameters.threshold,
		)

		self._logger.info('Returning state STREAMING_INITIALIZING for "%s"', request.id)
		yield pb_audio.DiscordRadioStreamResponse(
			chunks = [
				pb_audio.DiscordRadioStreamChunk(
					state_change = pb_audio.StateChange(
						old_state = pb_audio.StateChange.State.IDLE,
						new_state = pb_audio.StateChange.State.STREAMING_INITIALIZING,
					)
				)
			]
		)

		self._logger.info('Getting radio driver for "%s"', request.id)
		driver: Optional[backend.radio.driver.Driver] = None
		async for response in self._analysis_engine.get_radio_driver(request.id, parameters):
			percent = int(response.current / response.max * 100)
			self._logger.info('Loading progress: %s%%', percent)
			yield pb_audio.DiscordRadioStreamResponse(
				chunks = [
					pb_audio.DiscordRadioStreamChunk(
						state_change = pb_audio.StateChange(
							old_state = pb_audio.StateChange.State.STREAMING_INITIALIZING,
							new_state = pb_audio.StateChange.State.STREAMING_INITIALIZING,
							min_progress = 0,
							max_progress = 100,
							new_progress = percent,
						)
					)
				]
			)
			if response.payload is not None:
				driver = response.payload
				break

		if driver is None:
			return

		opus_encoder = backend.radio.driver.OpusChunkedTranscoder()

		riff_generator = driver.run()
		opus_generator = opus_encoder.transcode(riff_generator)

		self._logger.info('Returning state STREAMING for "%s"', request.id)
		yield pb_audio.DiscordRadioStreamResponse(
			chunks = [
				pb_audio.DiscordRadioStreamChunk(
					state_change = pb_audio.StateChange(
						old_state = pb_audio.StateChange.State.STREAMING_INITIALIZING,
						new_state = pb_audio.StateChange.State.STREAMING,
					)
				)
			]
		)

		current_chunk_index = 0
		async for chunk_bytes in opus_generator:
			chunk_length = len(chunk_bytes)
			max_chunk_size = 1 * 1024 * 1024  # 1 MiB
			total_chunks = math.ceil(chunk_length / max_chunk_size)
			for i in range(total_chunks):
				chunk_beginning = max_chunk_size * i
				chunk_end = (max_chunk_size * (i + 1)) if (i < total_chunks - 1) else chunk_length
				current_chunk_bytes = chunk_bytes[chunk_beginning:chunk_end]
				current_chunk_len = len(current_chunk_bytes)
				self._logger.info('Returning Opus chunk of %s KiB for "%s"', current_chunk_len // 1024, request.id)
				yield pb_audio.DiscordRadioStreamResponse(
					chunks = [
						pb_audio.DiscordRadioStreamChunk(
							state_change = pb_audio.StateChange(
								old_state = pb_audio.StateChange.State.STREAMING,
								new_state = pb_audio.StateChange.State.STREAMING,
							),
							chunk = pb_audio.OpusChunk(
								chunk = pb_audio.ArbitraryAudioChunk(
									total_chunk_count = 18446744073709551615,
									current_chunk_index = current_chunk_index,
									data_bytes = current_chunk_bytes,
								)
							),
						)
					]
				)
				current_chunk_index += 1

		self._logger.info('Returning state STREAMING_DONE for "%s"', request.id)
		yield pb_audio.DiscordRadioStreamResponse(
			chunks = [
				pb_audio.DiscordRadioStreamChunk(
					state_change = pb_audio.StateChange(
						old_state = pb_audio.StateChange.State.STREAMING,
						new_state = pb_audio.StateChange.State.STREAMING_DONE,
					)
				)
			]
		)

	async def YoutubeUrlForSpotifyID(
		self,
		request: pb_audio.YoutubeUrlForSpotifyIDRequest,
		_,
	) -> pb_audio.YoutubeUrlForSpotifyIDResponse:
		self._logger.info('Getting Youtube URL for Spotify ID "%s"', request.id)
		track_info = await self._analysis_engine.get_track_info_for_spotify_id(request.id)
		song_title = track_info['name']
		artist = track_info['artists'][0]['name']
		song_duration = track_info['duration_ms'] / 1000
		youtube_link = await self._analysis_engine.get_youtube_link(f'{artist} - {song_title}', song_duration)
		if youtube_link is None:
			raise Exception(f'Unable to find youtube link for spotify ID {request.id}')
		return pb_audio.YoutubeUrlForSpotifyIDResponse(url = youtube_link)

	async def YoutubeAudioData(
		self,
		request: pb_audio.YoutubeAudioDataRequest,
		_,
	) -> AsyncIterable[pb_audio.YoutubeAudioDataResponse]:
		self._logger.info('Getting Youtube audio data for URL "%s"', request.url)
		# try:
		audio_bytes: Optional[bytes] = None
		async for response in self._analysis_engine.get_wav_data_for_youtube_link(request.url):
			if response.payload is not None:
				audio_bytes = response.payload
			else:
				self._logger.error('Youtube audio data response was empty')
		if audio_bytes is None:
			return
		total_length = len(audio_bytes)
		self._logger.info('Resampling %s bytes', total_length)
		max_chunk_size = 1 * 1024 * 1024  # 1 MiB

		audio_chunks: List[bytes] = []
		total_chunks = math.ceil(total_length / max_chunk_size)
		for i in range(total_chunks):
			chunk_beginning = max_chunk_size * i
			chunk_end = (max_chunk_size * (i + 1)) if (i < total_chunks - 1) else total_length
			current_bytes = audio_bytes[chunk_beginning:chunk_end]
			audio_chunks.append(current_bytes)

		self._logger.info('Resampling done')

		chunk_count = len(audio_chunks)
		for (ix, chunk) in enumerate(audio_chunks):
			self._logger.info('Sending chunk %s out of %s', ix + 1, chunk_count)
			yield pb_audio.YoutubeAudioDataResponse(
				chunk = pb_audio.ArbitraryAudioChunk(
					total_chunk_count = chunk_count,
					current_chunk_index = ix,
					data_bytes = chunk,
				)
			)

		self._logger.info('All Youtube audio data sent')
		# except:
		# 	yield pb_audio.YoutubeAudioDataResponse(error = pb_audio.YoutubeFailureReason.TRANSPORT_FAILURE)
