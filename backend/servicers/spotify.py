# generated stubs are typed for synchronous gRPC
# pylint: disable=invalid-overridden-method

from typing import List
import json
import logging

import freem_protocols.freem_box.spotify_pb2 as pb_spotify
import freem_protocols.freem_box.spotify_pb2_grpc as pb_spotify_svcs
import grpc.aio

import backend.analysis


class SpotifyServicer(pb_spotify_svcs.SpotifyServiceServicer):
	def __init__(self, server: grpc.aio.Server, analysis_engine: backend.analysis.AnalysisEngine) -> None:
		super().__init__()
		pb_spotify_svcs.add_SpotifyServiceServicer_to_server(self, server)

		self._analysis_engine = analysis_engine
		self._logger = logging.getLogger('grpc-service-spotify')

	async def Search(
		self,
		request: pb_spotify.SpotifySearchRequest,
		_,
	) -> pb_spotify.SpotifySearchResult:
		def _error(error_type: pb_spotify.SpotifyFailureReason):
			return pb_spotify.SpotifySearchResult(failure_reason = error_type)  # type: ignore

		if (
			request.query is None
			or len(request.query) > 100
			or len(request.query) == 0
			or request.limit_count is None
			or request.limit_count < 1
			or request.limit_count > 50
		):
			self._logger.error('Invalid search request')
			return _error(pb_spotify.SpotifyFailureReason.API_REQUEST_REJECTED)  # type: ignore

		self._logger.info('Searching for "%s"', request.query)
		results = (await self._analysis_engine.get_spotify_results_for_query(request.query, request.limit_count))[
			'tracks'
		]['items']

		polished_results: List[pb_spotify.SpotifySearchedSong] = []

		for result in results:
			polished_results.append(
				pb_spotify.SpotifySearchedSong(
					id = result['id'],
					author = ', '.join([artist['name'] for artist in result['artists']]),
					name = result['name'],
					length_seconds = result['duration_ms'] / 1000,
				)
			)

		self._logger.info('Returning %s results for "%s"', len(polished_results), request.query)
		return pb_spotify.SpotifySearchResult(
			songs = polished_results,
			failure_reason = pb_spotify.SpotifyFailureReason.NONE,
		)

	async def Analyze(self, request: pb_spotify.SpotifyAnalysisRequest, _) -> pb_spotify.SpotifyAnalysisResult:
		def _error(error_type: pb_spotify.SpotifyFailureReason):
			return pb_spotify.SpotifyAnalysisResult(failure_reason = error_type)  # type: ignore

		if request is None or len(request.id) > 40 or len(request.id) < 5:
			self._logger.error('Invalid analyze request')
			return _error(pb_spotify.SpotifyFailureReason.API_REQUEST_REJECTED)  # type: ignore

		self._logger.info('Fetching analysis for "%s"', request.id)
		analysis = await self._analysis_engine.get_analysis_for_spotify_id(request.id)
		self._logger.info('Fetching track info for "%s"', request.id)
		track_info = await self._analysis_engine.get_track_info_for_spotify_id(request.id)

		try:
			song_title = track_info['name']
			artists = [artist['name'] for artist in track_info['artists']]
			track_duration = track_info['duration_ms'] / 1000
			response = {
				'info': {
					'service': 'SPOTIFY',
					'id': request.id,
					'name': song_title,
					'title': song_title,
					'artist': ', '.join(artists),
					'preview_url': track_info['preview_url'],
					'url': track_info['external_urls']['spotify'],
					'duration': track_duration,
				},
				'analysis': {
					'sections': analysis['sections'],
					'bars': analysis['bars'],
					'beats': analysis['beats'],
					'tatums': analysis['tatums'],
					'segments': analysis['segments'],
				},
				'audio_summary': {'duration': track_duration},
			}
			response_json = json.dumps(response)
			self._logger.info('Returning analysis for "%s"', request.id)
			return pb_spotify.SpotifyAnalysisResult(
				analysis_json = response_json,
				failure_reason = pb_spotify.SpotifyFailureReason.NONE,
			)
		except KeyError:
			return _error(error_type = pb_spotify.SpotifyFailureReason.TRANSPORT_FAILURE)  # type: ignore

	async def Info(self, request: pb_spotify.SpotifyAnalysisRequest, _) -> pb_spotify.SpotifySearchedSong:
		self._logger.info('Fetching track info for "%s"', request.id)
		track_info = await self._analysis_engine.get_track_info_for_spotify_id(request.id)
		artists = [artist['name'] for artist in track_info['artists']]

		return pb_spotify.SpotifySearchedSong(
			id = request.id,
			author = ', '.join(artists),
			name = track_info['name'],
			length_seconds = track_info['duration_ms'] / 1000,
		)
