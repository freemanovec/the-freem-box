import asyncio
import logging

import backend
import backend.configuration


async def main() -> None:
	log_level = logging.INFO
	logging.root.setLevel(log_level)

	configuration = backend.configuration.Configuration()
	backend_handler = backend.Backend(configuration)
	await backend_handler.run()


if __name__ == '__main__':
	asyncio.run(main())
