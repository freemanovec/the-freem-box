from dataclasses import dataclass
from io import BytesIO
from typing import AsyncGenerator, Dict, List, Optional
import datetime
import hashlib
import logging
import os
import pickle
import typing

import aiohttp
import aiohttp.helpers
import pydub
import yt_dlp

from backend.analysis.parts import Analysis, AnalysisParameters
from backend.configuration import Configuration
from backend.radio.driver import Driver
import backend.utils


class AnalysisEngine:
	def __init__(self, configuration: Configuration):
		self._logger = logging.getLogger('analysis-engine')
		self._configuration = configuration
		self._bearer_token: Optional[str] = None
		self._bearer_token_creation_date: datetime.datetime = datetime.datetime.now()

	async def get_analysis_for_spotify_id(self, spotify_id: str) -> Dict:
		id_hash = hashlib.sha256(spotify_id.encode('utf-8')).hexdigest()
		cache_directory = 'cache/analysis'
		if not os.path.isdir(cache_directory):
			os.makedirs(cache_directory)
		target_filename = f'{cache_directory}/{id_hash}.pickle'
		if not os.path.isfile(target_filename):
			self._logger.info('Getting analysis for %s for the first time', spotify_id)
			await self._check_spotify_token()
			target_url = (
				f'https://api.spotify.com/v1/audio-analysis/{spotify_id}'  # TODO add spotify ID format validation
			)
			async with aiohttp.ClientSession() as session:
				async with session.get(
					target_url,
					headers = {'Authorization': f'Bearer {self._bearer_token}'},
				) as response:
					result = await response.json()
					with open(target_filename, 'wb') as f:
						pickle.dump(result, f)
		else:
			self._logger.info('Getting analysis for %s from cache', spotify_id)
		with open(target_filename, 'rb') as f:
			return pickle.load(f)

	async def get_track_info_for_spotify_id(self, spotify_id: str) -> Dict:
		id_hash = hashlib.sha256(spotify_id.encode('utf-8')).hexdigest()
		cache_directory = 'cache/track'
		if not os.path.isdir(cache_directory):
			os.makedirs(cache_directory)
		target_filename = f'{cache_directory}/{id_hash}.pickle'
		if not os.path.isfile(target_filename):
			self._logger.info('Getting track info for %s for the first time', spotify_id)
			await self._check_spotify_token()
			target_url = f'https://api.spotify.com/v1/tracks/{spotify_id}'
			async with aiohttp.ClientSession() as session:
				async with session.get(
					target_url,
					headers = {'Authorization': f'Bearer {self._bearer_token}'},
				) as response:
					result = await response.json()
					with open(target_filename, 'wb') as f:
						pickle.dump(result, f)
		else:
			self._logger.info('Getting track info for %s from cache', spotify_id)
		with open(target_filename, 'rb') as f:
			return pickle.load(f)

	async def get_spotify_results_for_query(self, query: str, limit: int) -> Dict:
		await self._check_spotify_token()
		target_url = 'https://api.spotify.com/v1/search'
		async with aiohttp.ClientSession() as session:
			async with session.get(
				target_url,
				params = {'q': query, 'type': 'track', 'limit': limit},
				headers = {'Authorization': f'Bearer {self._bearer_token}'},
			) as response:
				result = await response.json()
				return result

	async def _check_spotify_token(self) -> None:
		if self._bearer_token is None or (
			typing.cast(datetime.timedelta, datetime.datetime.now() - self._bearer_token_creation_date).total_seconds()
			> 300
		):
			self._bearer_token = await self._get_spotify_bearer_token()
			self._bearer_token_creation_date = datetime.datetime.now()

	async def _get_spotify_bearer_token(self) -> str:  # TODO add bearer token refresh on failed request
		target_url = 'https://accounts.spotify.com/api/token'
		async with aiohttp.ClientSession(
			auth = aiohttp.helpers.BasicAuth(self._configuration.spotify_client, self._configuration.spotify_secret)
		) as session:
			async with session.post(
				target_url,
				data = {'grant_type': 'client_credentials'},
				headers = {'Content-Type': 'application/x-www-form-urlencoded'},
			) as response:
				result = await response.json()
				try:
					bearer_token: str = result['access_token']
					return bearer_token
				except KeyError as e:
					self._logger.fatal('Invalid response from Spotfy bearer token provisioner (%s): %s', e, result)
					raise

	async def get_youtube_link(self, name: str, target_length: float) -> Optional[str]:
		name_hash = hashlib.sha256(f'{name} (length {target_length})'.encode('utf-8')).hexdigest()
		cache_directory = 'cache/yt_search'
		if not os.path.isdir(cache_directory):
			os.makedirs(cache_directory)
		target_filename = f'{cache_directory}/{name_hash}.pickle'

		if not os.path.isfile(target_filename):
			self._logger.info(
				'Getting YouTube search results for %s (length %s) for the first time',
				name,
				target_length,
			)
			target_url = f'http://{self._configuration.searcher_host}'

			@dataclass
			class Hit:
				id: str
				length: float

			hits: List[Hit] = []
			async with aiohttp.ClientSession() as session:
				async with session.get(target_url, params = {'q': name}) as response:
					result = await response.json()
					try:
						for hit in result:
							hits.append(Hit(hit['id'], hit['seconds']))
					except KeyError as e:
						self._logger.fatal('Invalid response from youtube searcher (%s): %s', e, result)
						raise
				async with session.get(target_url, params = {'q': f'{name} lyrics'}) as response:
					result = await response.json()
					try:
						for hit in result:
							hits.append(Hit(hit['id'], hit['seconds']))
					except KeyError as e:
						self._logger.fatal('Invalid response from youtube searcher (%s): %s', e, result)
						raise
			best_hit = None
			for hit in hits:
				if best_hit is None:
					best_hit = hit
				else:
					absolute_difference_current = abs(target_length - hit.length)
					absolute_difference_best = abs(target_length - best_hit.length)
					if absolute_difference_current < absolute_difference_best:
						best_hit = hit
			if best_hit == None:
				return None
			full_link = f'https://www.youtube.com/watch?v={best_hit.id}'  # type: ignore
			with open(target_filename, 'wb') as f:
				pickle.dump(full_link, f)
		else:
			self._logger.info('Getting YouTube search results for %s (length %s) from cache', name, target_length)

		with open(target_filename, 'rb') as f2:
			return pickle.load(f2)

	async def get_audio_data_for_youtube_link(
		self,
		youtube_link: str,
	) -> bytes:  # TODO get permanent audio file caching
		link_hash = hashlib.sha256(youtube_link.encode('utf-8')).hexdigest()
		cache_directory = 'cache/audio'
		if not os.path.isdir(cache_directory):
			os.makedirs(cache_directory)
		target_filename = f'{cache_directory}/{link_hash}.m4a'

		if not os.path.isfile(target_filename):
			self._logger.info('Getting audio data for %s for the first time', youtube_link)
			with yt_dlp.YoutubeDL(
				{
					'outtmpl': target_filename,
					'format': 'bestaudio/best',
					'max_downloads': 1,
				}
			) as youtube_downloader:
				youtube_downloader.download([youtube_link])
		else:
			self._logger.info('Getting audio data for %s from cache', youtube_link)
		with open(target_filename, 'rb') as f:
			return f.read()

	async def get_wav_data_for_youtube_link(
		self,
		youtube_link: str,
	) -> AsyncGenerator[backend.utils.ProgressableResponse[bytes], None]:
		link_hash = hashlib.sha256(youtube_link.encode('utf-8')).hexdigest()
		cache_directory = 'cache/audio'
		target_filename = f'{cache_directory}/{link_hash}.wav'
		yield backend.utils.ProgressableResponse(0, 5, 0, None)
		if not os.path.isfile(target_filename):
			self._logger.info('Getting WAV data for %s for the first time', youtube_link)
			yield backend.utils.ProgressableResponse(0, 5, 1, None)
			m4a_bytes = await self.get_audio_data_for_youtube_link(  # dumb_style_checker: disable=word_as_a_digit
				youtube_link
			)
			yield backend.utils.ProgressableResponse(0, 5, 2, None)
			with BytesIO(m4a_bytes) as f:  # dumb_style_checker: disable=word_as_a_digit
				track = pydub.AudioSegment.from_file(f)
				yield backend.utils.ProgressableResponse(0, 5, 3, None)
				with open(target_filename, 'wb') as wav_bytes:
					track.export(wav_bytes, format = 'wav')
					yield backend.utils.ProgressableResponse(0, 5, 4, None)
		else:
			self._logger.info('Getting WAV data for %s from cache', youtube_link)
		with open(target_filename, 'rb') as cached_f:
			yield backend.utils.ProgressableResponse(0, 5, 5, cached_f.read())

	async def get_complete_analysis(self, spotify_id: str) -> Dict:
		analysis = await self.get_analysis_for_spotify_id(spotify_id)
		track_info = await self.get_track_info_for_spotify_id(spotify_id)
		song_title = track_info['name']
		artists = [artist['name'] for artist in track_info['artists']]
		track_duration = track_info['duration_ms'] / 1000

		analysis = {
			'info': {
				'service': 'SPOTIFY',
				'id': spotify_id,
				'name': song_title,
				'title': song_title,
				'artist': ', '.join(artists),
				'main_artist': artists[0],
				'preview_url': track_info['preview_url'],
				'url': track_info['external_urls']['spotify'],
				'duration': track_duration,
			},
			'analysis': {
				'sections': analysis['sections'],
				'bars': analysis['bars'],
				'beats': analysis['beats'],
				'tatums': analysis['tatums'],
				'segments': analysis['segments'],
			},
			'audio_summary': {'duration': track_duration},
		}

		return analysis

	async def get_radio_driver(
		self,
		spotify_id: str,
		parameters: AnalysisParameters,
		external_audio: Optional[str] = None,
	) -> AsyncGenerator[backend.utils.ProgressableResponse[Driver], None]:
		yield backend.utils.ProgressableResponse(0, 178, 0, None)
		radio_hash = hashlib.sha256(
			f'{spotify_id}_{parameters.get_hashable_parameters()}'.encode('utf-8')
		).hexdigest()
		cache_directory = 'cache/driver'
		if not os.path.isdir(cache_directory):
			os.makedirs(cache_directory)
		yield backend.utils.ProgressableResponse(0, 178, 1, None)

		target_filename = f'{cache_directory}/{radio_hash}.pickle'
		if not os.path.isfile(target_filename):
			self._logger.info('Getting radio for %s (parameters %s)', spotify_id, parameters)

			analysis_dict = await self.get_complete_analysis(spotify_id)
			yield backend.utils.ProgressableResponse(0, 178, 2, None)
			song_title = analysis_dict['info']['name']
			artist = analysis_dict['info']['main_artist']
			song_duration = analysis_dict['info']['duration']
			youtube_link = (
				await self.get_youtube_link(f'{artist} - {song_title}', song_duration)
				if external_audio is None
				else external_audio
			)
			if youtube_link == None:
				return
			yield backend.utils.ProgressableResponse(0, 178, 3, None)
			audio_bytes: Optional[bytes] = None
			async for response_yt in self.get_wav_data_for_youtube_link(typing.cast(str, youtube_link)):
				yield backend.utils.ProgressableResponse(0, 178, 4 + response_yt.current, None)
				if response_yt.payload is not None:
					audio_bytes = response_yt.payload
					break
			if audio_bytes == None:
				return
			yield backend.utils.ProgressableResponse(0, 178, 10, None)
			analysis = Analysis(analysis_dict)
			yield backend.utils.ProgressableResponse(0, 178, 11, None)
			result = analysis.analyze(parameters)
			yield backend.utils.ProgressableResponse(0, 178, 12, None)
			driver = Driver(typing.cast(bytes, audio_bytes), analysis, result)
			yield backend.utils.ProgressableResponse(0, 178, 13, None)
			async for response_dr in driver.prepare():  # max progress 160
				yield backend.utils.ProgressableResponse(0, 178, 14 + response_dr.current, None)

			yield backend.utils.ProgressableResponse(0, 178, 175, None)
			with open(target_filename, 'wb') as f:
				pickle.dump(driver, f)
				yield backend.utils.ProgressableResponse(0, 178, 176, None)
		else:
			yield backend.utils.ProgressableResponse(0, 178, 175, None)
			self._logger.info('Getting radio for %s (parameters %s) from cache', spotify_id, parameters)
		with open(target_filename, 'rb') as f:
			yield backend.utils.ProgressableResponse(0, 178, 177, None)
			loaded: Driver = pickle.load(f)
			yield backend.utils.ProgressableResponse(0, 178, 178, loaded)
