from typing import Any, Dict, List
import functools
import logging
import math


class AnalysisParameters:  # TODO change to dataclass
	def __init__(
		self,
		timbre_weight: float = 1,
		pitch_weight: float = 10,
		loud_start_weight: float = 1,
		loud_max_weight: float = 1,
		duration_weight: float = 100,
		confidence_weight: float = 1,
		just_backwards: bool = False,
		just_long_branches: bool = False,
		min_long_branch: float = 1,
		current_threshold: float = 30,
		add_last_edge: bool = True,
		max_branches: int = 4,
		max_branch_threshold: float = 80,
		jump_threshold_min: float = 0.48,
		jump_threshold_max: float = 0.73,
		jump_threshold_increment: float = 0.08,
	) -> None:
		self.timbre_weight = timbre_weight
		self.pitch_weight = pitch_weight
		self.loud_start_weight = loud_start_weight
		self.loud_max_weight = loud_max_weight
		self.duration_weight = duration_weight
		self.confidence_weight = confidence_weight
		self.just_backwards = just_backwards
		self.just_long_branches = just_long_branches
		self.min_long_branch = min_long_branch
		self.current_threshold = current_threshold
		self.add_last_edge = add_last_edge
		self.max_branches = max_branches
		self.max_branch_threshold = max_branch_threshold
		self.jump_threshold_min = jump_threshold_min
		self.jump_threshold_max = jump_threshold_max
		self.jump_threshold_increment = jump_threshold_increment

	def get_hashable_parameters(self) -> str:
		return f'{self.timbre_weight}_{self.pitch_weight}_{self.loud_start_weight}_{self.loud_max_weight}_{self.duration_weight}_{self.confidence_weight}_{self.just_backwards}_{self.just_long_branches}_{self.min_long_branch}_{self.current_threshold}_{self.add_last_edge}_{self.max_branches}_{self.max_branch_threshold}_{self.jump_threshold_min}_{self.jump_threshold_max}_{self.jump_threshold_increment}'

	def __str__(self) -> str:
		return f'AnalysisParameters ({self.current_threshold=}, {self.just_backwards=}, {self.just_long_branches=}, {self.jump_threshold_min=}, {self.jump_threshold_max=}, {self.jump_threshold_increment=})'

	def __repr__(self) -> str:
		return str(self)


class AnalysisResult:
	def __init__(self) -> None:
		self.total_beats: int = 0
		self.longest_reach: float = 0
		self.last_branch_point: float = 0
		self.count: float = 0
		self.song_duration: float = 0


class Analysis:
	def __init__(self, analysis_dict: Dict[str, Any]) -> None:
		self._logger = logging.getLogger('analysis-part')

		analysis = analysis_dict['analysis']
		track = analysis_dict['info']

		self.bars = analysis['bars']
		self.beats = analysis['beats']
		self.sections = analysis['sections']
		self.segments = analysis['segments']
		self.filtered_segments: List[Any] = []  # TODO finish typing this madness
		self.tatums = analysis['tatums']
		self.song_duration = track['duration']
		self.analysis_parameters = AnalysisParameters()
		self.song_id = track['id']
		self.long_name = f"{track['main_artist']} - {track['title']}"

		self._analyzeShallow(track)

	def analyze(self, parameters: AnalysisParameters) -> AnalysisResult:
		self.analysis_parameters = parameters
		analysis_result = self._analyzeDeep(parameters)
		return analysis_result

	def _analyzeShallow(self, track: Dict[str, Any]) -> None:
		self._logger.debug('Performing shallow analysis on %s', self.long_name)
		type_lists = [self.sections, self.bars, self.beats, self.tatums, self.segments]

		for type_list in type_lists:
			for (index, value) in enumerate(type_list):
				value['track'] = track
				value['which'] = index

				if index > 0:
					value['prev'] = type_list[index - 1]
				else:
					value['prev'] = None

				if index < (len(type_list) - 1):
					value['next'] = type_list[index + 1]
				else:
					value['next'] = None

		self._connectQuanta(self.sections, self.bars)
		self._connectQuanta(self.bars, self.beats)
		self._connectQuanta(self.beats, self.tatums)
		self._connectQuanta(self.tatums, self.segments)

		self._connectFirstOverlappingSegment(self.bars)
		self._connectFirstOverlappingSegment(self.beats)
		self._connectFirstOverlappingSegment(self.tatums)

		self._connectAllOverlappingSegments(self.bars)
		self._connectAllOverlappingSegments(self.beats)
		self._connectAllOverlappingSegments(self.tatums)

		self._filterSegments()

		self._logger.debug('Shallow analysis completed on %s', self.long_name)

	def _analyzeDeep(self, parameters: AnalysisParameters) -> AnalysisResult:
		self._logger.debug('Performing deep analysis on %s', self.long_name)
		result = AnalysisResult()

		self._precalculateNearestNeighbors(
			parameters.max_branches,
			parameters.max_branch_threshold,
			parameters,
		)
		result.count = self._collectNearestNeighbors(parameters.current_threshold, parameters)
		self._postProcessNearestNeighbors(parameters, result)
		result.song_duration = self.song_duration
		self._trimUnecessaryRemnains()

		self._logger.debug('Deep analysis completed on %s', self.long_name)
		return result

	def _trimUnecessaryRemnains(self):
		type_lists = [self.sections, self.bars, self.beats, self.tatums, self.segments]
		for type_list in type_lists:
			for (_, value) in enumerate(type_list):
				value['track'] = None
		self.sections = None
		self.bars = None
		self.tatums = None
		self.segments = None
		self.filtered_segments = None
		for beat in self.beats:
			beat['next'] = None
			beat['prev'] = None
			beat['overlappingSegments'] = None
			beat['children'] = None
			beat['parent'] = None
			beat['oseg'] = None
			beat['all_neighbors'] = None
			if len(beat['neighbors']) > 0:
				for _, neighbor in enumerate(beat['neighbors']):
					neighbor['src'] = neighbor['src']['which']
					neighbor['dest'] = neighbor['dest']['which']

	def _connectQuanta(self, qparents: List[Any], qchildren: List[Any]):
		last = 0
		for i in range(len(qparents)):
			qparent = qparents[i]
			qparent['children'] = []

			for j in range(last, len(qchildren)):
				qchild = qchildren[j]
				if (qchild['start'] >= qparent['start']) and (qchild['start'] < (qparent['start'] + qparent['duration'])):
					qchild['parent'] = qparent
					qchild['indexInParent'] = len(qparent['children'])
					qparent['children'].append(qchild)
					last = j
				elif qchild['start'] > qparent['start']:
					break

	def _connectFirstOverlappingSegment(self, quanta: List[Any]):
		last = 0
		segs = self.segments

		for i in range(len(quanta)):
			q = quanta[i]

			for j in range(last, len(segs)):
				qseg = segs[j]

				if qseg['start'] >= q['start']:
					q['oseg'] = qseg
					last = j
					break

	def _connectAllOverlappingSegments(self, quanta: List[Any]):
		last = 0
		segs = self.segments

		for i in range(len(quanta)):
			q = quanta[i]
			q['overlappingSegments'] = []

			for j in range(last, len(segs)):
				qseg = segs[j]

				if (qseg['start'] + qseg['duration']) < q['start']:
					continue

				if qseg['start'] > (q['start'] + q['duration']):
					break

				last = j
				q['overlappingSegments'].append(qseg)

	def _filterSegments(self):
		threshold = 0.3
		fsegs = []
		fsegs.append(self.segments[0])

		for i in range(1, len(self.segments)):
			seg = self.segments[i]
			last = fsegs[len(fsegs) - 1]

			if self._is_similar(seg, last) and seg['confidence'] < threshold:
				fsegs[len(fsegs) - 1]['duration'] += seg['duration']
			else:
				fsegs.append(seg)

		self.filtered_segments = fsegs

	def _is_similar(self, seg1, seg2):
		threshold = 1
		distance = self._timbral_distance(seg1, seg2)
		return distance < threshold

	def _timbral_distance(self, seg1, seg2):
		return self._euclidean_distance(seg1['timbre'], seg2['timbre'])

	def _euclidean_distance(self, v1: List[float], v2: List[float]):
		sum_ = 0.0

		for i in range(len(v1)):
			delta = v2[i] - v1[i]
			sum_ += delta * delta

		return math.sqrt(sum_)

	def _precalculateNearestNeighbors(
		self,
		maxNeighbors: int,
		maxThreshold: float,
		parameters: AnalysisParameters,
	):
		if 'all_neighbors' in self.beats[0]:
			return

		all_edges: List[Any] = []
		for qi in range(len(self.beats)):
			q1 = self.beats[qi]
			self._calculateNearestNeighborsForQuantum(maxNeighbors, maxThreshold, q1, parameters, all_edges)

	def _calculateNearestNeighborsForQuantum(
		self,
		maxNeighbors: int,
		maxThreshold: float,
		q1: Dict[str, Any],
		parameters: AnalysisParameters,
		all_edges: List[Any],
	):
		edges = []
		id_ = 0

		for i in range(len(self.beats)):
			if i == q1['which']:
				continue

			q2 = self.beats[i]
			sum_ = 0

			for j in range(len(q1['overlappingSegments'])):
				seg1 = q1['overlappingSegments'][j]
				distance = 100

				if j < len(q2['overlappingSegments']):
					seg2 = q2['overlappingSegments'][j]
					if seg1['which'] == seg2['which']:
						distance = 100
					else:
						distance = self._get_seg_distance(seg1, seg2, parameters)

				sum_ += distance

			if 'indexInParent' not in q1 and 'indexInParent' in q2:
				pdistance = 100
			elif 'indexInParent' in q1 and not 'indexInParent' in q2:
				pdistance = 100
			elif 'indexInParent' not in q1 and 'indexInParent' not in q2:
				pdistance = 0
			else:
				pdistance = 0 if q1['indexInParent'] == q2['indexInParent'] else 100
			totalDistance = sum_ / len(q1['overlappingSegments']) + pdistance
			if totalDistance < maxThreshold:
				edge = {'id': id_, 'src': q1, 'dest': q2, 'distance': totalDistance, 'curve': None, 'forced': False}
				edges.append(edge)
				id_ += 1

		def edgeSort(a, b):
			if a['distance'] > b['distance']:
				return 1
			elif b['distance'] > a['distance']:
				return -1
			return 0

		edges.sort(key = functools.cmp_to_key(edgeSort))

		q1['all_neighbors'] = []
		for i in range(min(maxNeighbors, len(edges))):
			edge = edges[i]
			q1['all_neighbors'].append(edge)
			edge['id'] = len(all_edges)
			all_edges.append(edge)

	def _get_seg_distance(self, seg1: Dict[str, Any], seg2: Dict[str, Any], parameters: AnalysisParameters):
		timbre = self._seg_distance(seg1, seg2, 'timbre')
		pitch = self._seg_distance(seg1, seg2, 'pitches')
		sloudStart = abs(seg1['loudness_start'] - seg2['loudness_start'])
		sloudMax = abs(seg1['loudness_max'] - seg2['loudness_max'])
		duration = abs(seg1['duration'] - seg2['duration'])
		confidence = abs(seg1['confidence'] - seg2['confidence'])
		distance = (
			timbre * parameters.timbre_weight
			+ pitch * parameters.pitch_weight
			+ sloudStart * parameters.loud_start_weight
			+ sloudMax * parameters.loud_max_weight
			+ duration * parameters.duration_weight
			+ confidence * parameters.confidence_weight
		)
		return distance

	def _seg_distance(self, seg1: Dict[str, Any], seg2: Dict[str, Any], field: str):
		return self._euclidean_distance(seg1[field], seg2[field])

	def _collectNearestNeighbors(self, maxThreshold: float, parameters: AnalysisParameters):
		branchingCount = 0
		for qi in range(len(self.beats)):
			q1 = self.beats[qi]
			q1['neighbors'] = self._extractNearestNeighbors(q1, maxThreshold, parameters)
			if len(q1['neighbors']) > 0:
				branchingCount += 1
		return branchingCount

	def _extractNearestNeighbors(self, q: Dict[str, Any], maxThreshold: float, parameters: AnalysisParameters):
		neighbors = []

		for i in range(len(q['all_neighbors'])):
			neighbor = q['all_neighbors'][i]

			if parameters.just_backwards and (neighbor['dest']['which'] > q['which']):
				continue

			if parameters.just_long_branches and (
				abs(neighbor['dest']['which'] - q['which']) < parameters.min_long_branch
			):
				continue

			distance = neighbor['distance']
			if distance <= maxThreshold:
				neighbors.append(neighbor)
		return neighbors

	def _postProcessNearestNeighbors(self, parameters: AnalysisParameters, result: AnalysisResult):
		if parameters.add_last_edge:
			if self._longestBackwardBranch() < 50:
				self._insertBestBackwardBranch(parameters.current_threshold, 65)
			else:
				self._insertBestBackwardBranch(parameters.current_threshold, 55)

		self._calculateReachability()
		lastBranchPoint = self._findBestLastBeat(result)
		self._filterOutBadBranches(lastBranchPoint)
		result.last_branch_point = lastBranchPoint

	def _longestBackwardBranch(self):
		longest = 0
		quanta = self.beats
		for i in range(len(quanta)):
			q = quanta[i]
			for j in range(len(q['neighbors'])):
				neighbor = q['neighbors'][j]
				which = neighbor['dest']['which']
				delta = i - which
				if delta > longest:
					longest = delta

		lbb = longest * 100 / len(quanta)
		return lbb

	def _insertBestBackwardBranch(self, threshold: float, maxThreshold: float):
		branches = []
		quanta = self.beats
		for i in range(len(quanta)):
			q = quanta[i]
			for j in range(len(q['all_neighbors'])):
				neighbor = q['all_neighbors'][j]

				which = neighbor['dest']['which']
				thresh = neighbor['distance']
				delta = i - which
				if delta > 0 and thresh < maxThreshold:
					percent = delta * 100 / len(quanta)
					edge = [percent, i, which, q, neighbor]
					branches.append(edge)

		if len(branches) == 0:
			self._logger.warning("Couldn't find a best branch to add final backward branch!")
			return

		def branchesSort(a, b):
			return a[0] - b[0]

		branches.sort(key = functools.cmp_to_key(branchesSort))
		branches.reverse()

		best = branches[0]
		bestQ = best[3]
		bestNeighbor = best[4]
		bestThreshold = bestNeighbor['distance']
		bestNeighbor['forced'] = True
		if bestThreshold > threshold:
			bestQ['neighbors'].append(bestNeighbor)

	def _calculateReachability(self):
		maxIter = 1000
		quanta = self.beats

		for qi in range(len(quanta)):
			q = quanta[qi]
			q['reach'] = len(quanta) - q['which']

		for _ in range(maxIter):
			changeCount = 0
			for qi in range(len(quanta)):
				q = quanta[qi]
				changed = False

				for i in range(len(q['neighbors'])):
					q2 = q['neighbors'][i]['dest']
					if q2['reach'] > q['reach']:
						q['reach'] = q2['reach']
						changed = True

				if qi < (len(quanta) - 1):
					q2 = quanta[qi + 1]
					if q2['reach'] > q['reach']:
						q['reach'] = q2['reach']
						changed = True

				if changed:
					changeCount += 1
					for j in range(q['which']):
						q2 = quanta[j]
						if q2['reach'] < q['reach']:
							q2['reach'] = q['reach']

			if changeCount == 0:
				break

	def _findBestLastBeat(self, result: AnalysisResult):
		reachThreshold = 50
		quanta = self.beats
		longest = 0
		longestReach = 0
		for i in range(len(quanta) - 1, -1, -1):
			q = quanta[i]
			distanceToEnd = len(quanta) - i

			reach = (q['reach'] - distanceToEnd) * 100 / len(quanta)

			if reach > longestReach and (len(q['neighbors']) > 0):
				longestReach = reach
				longest = i
				if reach >= reachThreshold:
					break

		result.total_beats = len(quanta)
		result.longest_reach = longestReach
		return longest

	def _filterOutBadBranches(self, lastIndex: int):
		quanta = self.beats
		for i in range(lastIndex):
			q = quanta[i]
			newList = []
			for j in range(len(q['neighbors'])):
				neighbor = q['neighbors'][j]
				if neighbor['dest']['which'] < lastIndex:
					newList.append(neighbor)
			q['neighbors'] = newList
