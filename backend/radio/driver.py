from enum import Enum
from io import BytesIO
from typing import AsyncGenerator, List, Optional, Tuple
import logging
import random
import wave

import librosa
import numpy

from backend.analysis.parts import Analysis, AnalysisResult
import backend.utils


class DrivingState(Enum):
	IDLE = 0
	ERRORED = 1
	SEARCHING_SPOTIFY = 2
	SEARCHICH_SPOTIFY_DONE = 3
	SEARCHING_YOUTUBE = 4
	SEARCHING_YOUTUBE_DONE = 5
	FETCHING_SPOTIFY_ANALYSIS = 6
	FETCHING_SPOTIFY_ANALYSIS_DONE = 7
	FETCHING_YOUTUBE_AUDIO = 8
	FETCHING_YOUTUBE_AUDIO_DONE = 9
	ENGINE_ANALYZING = 10
	ENGINE_ANALYZING_DONE = 11
	TRANSCODING = 12
	TRANSCODING_DONE = 13
	STREAMING_INITIALIZING = 14
	STREAMING = 15
	STREAMING_DONE = 16


class Driver:
	def __init__(self, audio_bytes: bytes, analysis: Analysis, analysis_result: AnalysisResult) -> None:
		self._analysis = analysis
		self._analysis_result = analysis_result
		self._audio_bytes = audio_bytes
		self._logger = logging.getLogger('radio-driver')
		self.wave_frames: List[bytes] = []
		self.channels = 0
		self.sample_rate = 0
		self.bits_per_sample = 0

	async def prepare(self) -> AsyncGenerator[backend.utils.ProgressableResponse[None], None]:
		with BytesIO(self._audio_bytes) as audio_bytes_io:
			self._logger.debug('Preparing audio for %s', self._analysis.long_name)
			yield backend.utils.ProgressableResponse(0, 150, 0, None)
			wf: wave.Wave_read = wave.open(audio_bytes_io)
			yield backend.utils.ProgressableResponse(0, 150, 30, None)
			total_frames = wf.getnframes()
			for i in range(total_frames):
				self.wave_frames.append(wf.readframes(1))
				if i % 100000 == 0:
					percent = int(i / total_frames * 100)
					yield backend.utils.ProgressableResponse(0, 160, 50 + percent, None)
					self._logger.info('Retrieving frames for %s - %s%%', self._analysis.long_name, percent)
			self.channels = wf.getnchannels()
			self.sample_rate = wf.getframerate()
			self.bits_per_sample = wf.getsampwidth() * 8
			yield backend.utils.ProgressableResponse(0, 160, 160, None)
			self._logger.debug(
				'Audio prepared for %s (wave frames: %s, channels: %s, sample rate: %s, bits per sample: %s)',
				self._analysis.long_name,
				len(self.wave_frames),
				self.channels,
				self.sample_rate,
				self.bits_per_sample,
			)

	async def run(self) -> AsyncGenerator[bytes, None]:
		# yield RIFF WAV header
		yield self.get_header()
		# get first tile
		tile_count = len(self._analysis.beats)
		current_tile_index = 0
		current_tile = self._analysis.beats[current_tile_index]
		track_duration = self._analysis_result.song_duration
		frame_count = min(len(self.wave_frames), track_duration * self.sample_rate)
		current_jump_chance = 0.18  # initial hardcoded change for a jump on the first branch
		generated_duration = 0
		# loop:
		while current_tile_index < tile_count:
			# get current tile range
			start_seconds = current_tile['start']
			start_percent = start_seconds / track_duration
			duration_seconds = current_tile['duration']
			generated_duration += duration_seconds
			end_seconds = start_seconds + duration_seconds
			end_percent = end_seconds / track_duration
			if end_percent >= 1:
				end_percent = 1
			# get frames in range
			start_index = int(
				frame_count * start_percent
			)  # TODO don't use youtube frame count as the max, calculate clamped max from the song duration to mitigate possible silence at the end of youtube videos
			end_index = int(frame_count * end_percent)
			# yield current tile range
			if 0 > start_index >= (len(self.wave_frames) - 1) or 0 > end_index >= (len(self.wave_frames) - 1):
				self._logger.error('Attempted to play a tile outside the WAV file!')
				start_index = 0
				end_index = 1
			tile_frames = self.wave_frames[start_index:end_index]
			for tile_frame in tile_frames:
				yield tile_frame

			next_tile_index = None
			# is current tile a branch?
			if len(current_tile['neighbors']) > 0:
				# should we jump?
				target_jump_threshold_value = self._analysis.analysis_parameters.jump_threshold_min + (
					random.random()
					* (
						self._analysis.analysis_parameters.jump_threshold_max
						- self._analysis.analysis_parameters.jump_threshold_min
					)
				)
				forced_branches = [branch for branch in current_tile['neighbors'] if branch['forced']]
				if len(forced_branches) > 0:
					self._logger.debug('Found a forced branch, will jump')
				if len(forced_branches) > 0 or current_jump_chance >= target_jump_threshold_value:
					# set current tile to branch target tile, set current tile index to the same
					chosen_neighbor = random.choice(
						forced_branches if len(forced_branches) > 0 else current_tile['neighbors']
					)
					chosen_next_tile_index = (chosen_neighbor['dest'] + 1) % tile_count
					jump_distance = abs(chosen_next_tile_index - current_tile_index)
					can_jump = True
					if self._analysis.analysis_parameters.just_backwards and chosen_next_tile_index > current_tile_index:
						can_jump = False
						self._logger.debug(
							'Ignoring branch, just backwards is on and the jump would be %s -> %s',
							current_tile_index,
							chosen_next_tile_index,
						)
					elif (
						self._analysis.analysis_parameters.just_long_branches
						and jump_distance < self._analysis.analysis_parameters.min_long_branch
					):
						can_jump = False
						self._logger.debug(
							'Ignoring branch, only long is on and the jump would be %s, less than the minimum %s',
							jump_distance,
							self._analysis.analysis_parameters.min_long_branch,
						)
					if can_jump:
						next_tile_index = (chosen_neighbor['dest'] + 1) % tile_count
						self._logger.debug('Jumping from %s to %s', current_tile_index, next_tile_index)
						current_jump_chance /= 3  # we jumped, cut the current jump chance to 1/3

				# else:
				# 	self.logger.debug(
				# 		f'Decided not to jump, prob was {current_jump_chance}, needed {target_jump_threshold_value}'
				# 	)
				current_jump_chance = (
					current_jump_chance + self._analysis.analysis_parameters.jump_threshold_increment
				) % self._analysis.analysis_parameters.jump_threshold_max
			# if we did not change tile, go to the next one
			if next_tile_index is None:
				# set next tile to the next one :D
				next_tile_index = (current_tile_index + 1) % tile_count

			current_tile = self._analysis.beats[next_tile_index]
			current_tile_index = next_tile_index

	def get_header(self) -> bytes:
		datasize = 4294967295 - 36
		output = (
			bytes('RIFF', 'ascii')
			+ (datasize + 36).to_bytes(4, 'little')
			+ bytes('WAVE', 'ascii')
			+ bytes('fmt ', 'ascii')
			+ (16).to_bytes(4, 'little')
			+ (1).to_bytes(2, 'little')
			+ (self.channels).to_bytes(2, 'little')
			+ (self.sample_rate).to_bytes(4, 'little')
			+ (self.sample_rate * self.channels * self.bits_per_sample // 8).to_bytes(4, 'little')
			+ (self.channels * self.bits_per_sample // 8).to_bytes(2, 'little')
			+ (self.bits_per_sample).to_bytes(2, 'little')
			+ bytes('data', 'ascii')
			+ datasize.to_bytes(4, 'little')
		)
		return output


class OpusChunkedTranscoder:
	def __init__(self) -> None:
		self.logger = logging.getLogger('trancoder-chunked-opus')

	async def transcode(
		self,
		riff_byte_provider: AsyncGenerator[bytes, None],
		target_sample_rate: int = 48000,
	) -> AsyncGenerator[bytes, None]:
		self.logger.info('Fetching RIFF header')

		rolling_buffer: bytearray = bytearray()
		got_header = False
		channels: Optional[int] = None
		sample_rate: Optional[int] = None
		bits_per_sample: Optional[int] = None
		bytes_per_sample: Optional[int] = None
		virtual_header: Optional[bytes] = None
		async for next_input_chunk in riff_byte_provider:
			rolling_buffer += next_input_chunk
			if not got_header:
				if len(rolling_buffer) >= 44:
					# got header in rolling buffer
					header_bytes: bytes = rolling_buffer[:44]
					channels, sample_rate, bits_per_sample, virtual_header = OpusChunkedTranscoder._parse_header(
						header_bytes
					)
					bytes_per_sample = bits_per_sample // 2
					# parsed header, cut from rolling buffer
					rolling_buffer = rolling_buffer[44:] if len(rolling_buffer) > 44 else bytearray()
					got_header = True
					self.logger.info('Got RIFF header 🎉')
				else:
					# don't have header yet, but buffer is not big enough yet to contain it
					continue
			else:
				if channels is None or virtual_header is None or sample_rate is None or bytes_per_sample is None:
					raise Exception('Out-of-order samples, missing header')

				# got header, carry on to encode Opus
				min_playable_length = 10 * sample_rate * bytes_per_sample * channels

				if len(rolling_buffer) > min_playable_length:
					self.logger.info('Chunk is now encodable at %s bytes', len(rolling_buffer))
					playable_chunk = rolling_buffer[:min_playable_length]
					rolling_buffer = rolling_buffer[min_playable_length:]
					playable_chunk_bytes = bytes(playable_chunk)
					virtual_riff_file_bytes = virtual_header + playable_chunk_bytes
					with BytesIO(virtual_riff_file_bytes) as virtual_riff_file:
						resampled = librosa.load(
							virtual_riff_file,
							sr = target_sample_rate * channels,
							mono = channels == 1,
						)  # multiplied by channels
						resampled_audio_data = resampled[0]
						if channels > 1:
							resampled_audio_data = numpy.average(resampled_audio_data, axis = 0)
						contiguous = numpy.ascontiguousarray(resampled_audio_data)
						contiguous *= 0.5  # prevent clipping
						ints = (contiguous * 32767).astype(numpy.int16)
						little_endian = ints.astype('<u2')
						# pylint doesn't see tostring() on ndarray, but it's there 🤷‍♀️
						resampled_buffer: bytes = little_endian.tostring()  # type: ignore
						self.logger.info('Encoding chunk done')
						yield resampled_buffer

	@staticmethod
	def _parse_header(header: bytes) -> Tuple[int, int, int, bytes]:
		bytes_channels = header[22:24]
		bytes_sample_rate = header[24:28]
		bytes_bits_per_sample = header[34:36]
		channels = int.from_bytes(bytes_channels, byteorder = 'little')
		sample_rate = int.from_bytes(bytes_sample_rate, byteorder = 'little')
		bits_per_sample = int.from_bytes(bytes_bits_per_sample, byteorder = 'little')
		virtual_header_bytes = (
			bytes('RIFF', 'ascii')
			+ (4294967295).to_bytes(4, 'little')
			+ bytes('WAVE', 'ascii')
			+ bytes('fmt ', 'ascii')
			+ (16).to_bytes(4, 'little')
			+ (1).to_bytes(2, 'little')
			+ (channels).to_bytes(2, 'little')
			+ (sample_rate).to_bytes(4, 'little')
			+ (sample_rate * channels * bits_per_sample // 8).to_bytes(4, 'little')
			+ (channels * bits_per_sample // 8).to_bytes(2, 'little')
			+ (bits_per_sample).to_bytes(2, 'little')
			+ bytes('data', 'ascii')
			+ (4294967295 - 36).to_bytes(4, 'little')
		)
		return (channels, sample_rate, bits_per_sample, virtual_header_bytes)
