from dataclasses import dataclass
from typing import Generic, Optional, TypeVar


T = TypeVar('T')


@dataclass
class ProgressableResponse(Generic[T]):
	min: int
	max: int
	current: int
	payload: Optional[T]
